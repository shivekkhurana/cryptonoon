#!/bin/bash

# Setup Script
# Sets up monit and xmr stak

# Author: Shivek Khurana
# Date: January 15, 2018


# clean up
sudo rm /etc/monit/conf-available/xmr-stak
sudo rm /etc/monit/conf-enabled/xmr-stak

# update deps on new machine
sudo apt update

# install miner dependencies

## go the straight way : aws, do, linode, scaleway
sudo apt install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev emacs dnsutils -y

## go the aptitude way : alibaba
# sudo apt install aptitude
# sudo aptitude install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev emacs -y

# build miner
mkdir xmr-stak/build
cd xmr-stak/build
cmake -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF .. # aws, do, linode, scaleway
# cmake -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF -DHWLOC_ENABLE=OFF .. # alibaba
make install

# copy binary folder to $HOME
mkdir ~/xmr-stak-bin
cp ~/cryptonoon/setup/xmr-stak/build/bin/* ~/xmr-stak-bin

# copy config and cpu
cd ~/xmr-stak-bin
cp ~/cryptonoon/setup/configs/sumo.txt config.txt
cp ~/cryptonoon/setup/cpus/aws/t2.micro.txt cpu.txt
cd ~

# add ip identifier
ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
sed -i "s/{#ip-address}/${ip//\./-}/g" ~/xmr-stak-bin/config.txt

# install monit
sudo apt install monit -y

# copy monit xmr-stak conf
sudo cp ~/cryptonoon/setup/monit/xmr-stak /etc/monit/conf-available

# symlink monit xmr-stak conf
sudo ln -s /etc/monit/conf-available/xmr-stak /etc/monit/conf-enabled

# create start script for monit to start minining
cd ~
touch start-miner.sh
## enable large page each time because the static conf doesn't quite work
echo "sudo sysctl -w vm.nr_hugepages=128; cd ~/xmr-stak-bin; ./xmr-stak &" > ~/start-miner.sh
chmod +x start-miner.sh

# start monit
sudo monit # this doesn't starts the http interface
sudo monit quit
sudo monit # close and restart does


